
Roboforces wiki repo.

---

## GitLab CI

This project's static Pages are built by GitLab CI, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at MkDocs [documentation](https://www.mkdocs.org/).

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change the `site_url` line
in your `mkdocs.yml`, from `"https://pages.gitlab.io/mkdocs/"` to
`site_url: "https://namespace.gitlab.io"`.

